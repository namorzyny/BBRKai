package_name := bbr_kai
package_version := 1.0

obj-m := tcp_bbr_kai.o

ifndef kernelver
	kernelver := $(shell uname -r)
endif

ifndef kernel_source_dir
	kernel_source_dir := /lib/modules/$(kernelver)/build
endif

ver := $(shell echo $(kernelver) | grep -o -P '^\d+\.\d+')

build:
	@wget -O - "https://raw.githubusercontent.com/torvalds/linux/v$(ver)/net/ipv4/tcp_bbr.c" | sed -f patches.txt > tcp_bbr_kai.c
	@make -C $(kernel_source_dir) M=$(shell pwd) modules

clean:
	@make -C $(kernel_source_dir) M=$(shell pwd) clean
	@rm *.c

dkms-install:
	@cp -r $(shell pwd) /usr/src/$(package_name)-$(package_version)
	@dkms add $(package_name)/$(package_version)
	@dkms build $(package_name)/$(package_version)
	@dkms install $(package_name)/$(package_version)

dkms-remove:
	@dkms remove $(package_name)/$(package_version) --all
	@rm -rf /usr/src/$(package_name)-$(package_version)
